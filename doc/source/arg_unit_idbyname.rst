| The unit of id. 
| This integer is 0 or the constant |CALCEPH_USE_NAIFID|.
| If the unit equals |CALCEPH_USE_NAIFID|, the NAIF identification numbering system is used for the id (:ref:`NAIF identification numbers` for the list). 
| If the unit equals 0, the old number system is used for the idr (see the list in the function |calceph_compute|). 
