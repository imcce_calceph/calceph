
This function returns the number of constants available in the header of the ephemeris file |eph|. 

The following example prints the number of available constants stored in the ephemeris file

.. include:: examples/multiple_getconstantcount.rst
