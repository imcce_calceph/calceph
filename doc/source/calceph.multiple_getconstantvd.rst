    
The values must be  floating-point or integer numbers, otherwise an error is reported. 

The following example prints the body radii of the earth stored in the ephemeris file

.. include:: examples/multiple_getconstantvd.rst