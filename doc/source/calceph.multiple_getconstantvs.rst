The values must be strings, otherwise an error is reported. 
 
.. ifconfig:: calcephapi in ('F90', 'F2003')

    Trailing blanks are added to each value.

The following example prints the units of the mission stored in the ephemeris file

.. include:: examples/multiple_getconstantvs.rst
