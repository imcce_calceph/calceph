

The library has a default mapping listed in the list :ref:`NAIF identification numbers`. 
The mapping name/id may be overriden by the text constants *NAIF_BODY_CODE* and *NAIF_BODY_NAME* in the 'SPICE' ephemeris files.




The following example prints the name of the Earth-Moon barycenter.

.. include:: examples/multiple_getnamebyidss.rst