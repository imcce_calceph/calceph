
This function returns the timescale of the ephemeris file |eph| :

  * 1 if the quantities of all bodies are expressed in the TDB time scale. 
  * 2 if the quantities of all bodies are expressed in the TCB time scale. 
  

The following example prints the time scale available in the ephemeris file

.. include:: examples/multiple_gettimescale.rst