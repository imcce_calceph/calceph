KPL/FK

\begindata

      FRAME_TFE-Y           = -140910
      FRAME_-140910_NAME        = 'TFE-Y'
      FRAME_-140910_CLASS       = 4
      FRAME_-140910_CLASS_ID    = -140910
      FRAME_-140910_CENTER      = -140
      TKFRAME_-140910_SPEC      = 'ANGLES'
      TKFRAME_-140910_RELATIVE  = 'DIF_SPACECRAFT'
      TKFRAME_-140910_ANGLES    = (    0.0,   0.0,  -90.0 )
      TKFRAME_-140910_AXES      = (    3,     2,      1   )
      TKFRAME_-140910_UNITS     = 'DEGREES'

      FRAME_TFE+Y           = -140920
      FRAME_-140920_NAME        = 'TFE+Y'
      FRAME_-140920_CLASS       = 4
      FRAME_-140920_CLASS_ID    = -140920
      FRAME_-140920_CENTER      = -140
      TKFRAME_-140920_SPEC      = 'ANGLES'
      TKFRAME_-140920_RELATIVE  = 'DIF_SPACECRAFT'
      TKFRAME_-140920_ANGLES    = (    0.0,   0.0,   90.0 )
      TKFRAME_-140920_AXES      = (    3,     2,      1   )
      TKFRAME_-140920_UNITS     = 'DEGREES'
