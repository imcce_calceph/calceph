This library is designed to access the binary planetary ephemeris files, 
such INPOPxx, JPL DExxx and SPICE ephemeris files.
This library provides a C Application Programming Interface (API) 
and, optionally, Fortran 77/2003, Python 2/3 and Octave/Matlab interfaces 
to be called by the application.
